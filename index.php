<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;

$app = new \Slim\App();
$app->get('/customers/{id}', function($request, $response, $array){
    $id = $request->getAttribute('id');
    $json = '{"1":"John", "2":"Jack","3":"Aviram","4":"Roni","5":"Alon"}';
    $array = json_decode($json, true); //the 'true' must be here

    if(array_key_exists($id, $array)){
        $name = $array[$id];
        return $response->write('Hello, '.$name);
    }
    else{
        return $response->write('Hello, this id does not exist. Please choose an id from 1 to 5');
    }
});

$app->get('/hello/{name}', function($request, $response,$args){
    return $response->write('Hello '.$args['name']);
});
///////////////////////////////////end of ex1

////////////Messages CRUD
$app->get('/messages', function($request, $response,$args){ //read
    $_message = new Message();
    $messages = $_message->all(); //מערך של אוביקטים 
    $payload = [];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at
        ];        
    }
    return $response->withStatus(200)->withJson($payload);
 });
 
 $app->post('/messages', function($request, $response,$args){ //create
    $message = $request->getParsedBodyParam('message', '');
    $user_id = $request->getParsedBodyParam('user_id', '');
    $_message = new Message();
    $_message->body = $message; 
    $_message->user_id = $user_id;
    $_message->save();
    if($_message->id){
        $payload = ['message_id'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400); 
    } 
   
 });
 
 $app->delete('/messages/{message_id}', function($request, $response,$args){ 
   $message = Message::find($args['message_id']);
   $message->delete(); 
   if($message->exists){
        return $response->withStatus(400);
    } else {
        return $response->withStatus(200);
    }
 
 });

 $app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message', '');
    $_message = Message::find($args['message_id']);
    //die("message id: ".$_message->id);
    $_message->body = $message;
    if($_message->save()){
        $payload = ['message_id'=>$_message->id, "result"=>"The message has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    } else{
        return $response->withStatus(400);  
    }
 
 });
 
 $app->post('/messages/bulk', function($request, $response, $args){
    $payload = $request->getParsedBody(); //convert the json to array
    Message::insert($payload); //include save
    return $response->withStatus(201)->withJson($payload);
      
 });

 ////////////Ex2 - users CRUD
 $app->get('/users', function($request, $response,$args){ //read
    $_user = new User();
    $users = $_user->all(); //מערך של אוביקטים 
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'username'=>$usr->username,
            'password'=>$usr->password,
            'email'=>$usr->email
        ];        
    }
    return $response->withStatus(200)->withJson($payload);
 });
 
 $app->post('/users', function($request, $response,$args){ //create
    $username = $request->getParsedBodyParam('username', '');
    $password = $request->getParsedBodyParam('password', '');
    $email = $request->getParsedBodyParam('email', '');
    $_user = new User();
    $_user->username = $username; 
    $_user->password = $password;
    $_user->email = $email;
    $_user->save();
    if($_user->id){
        $payload = ['user_id'=>$_user->id];
        return $response->withStatus(201)->withJson($payload);
    } else {
        return $response->withStatus(400); 
    } 
 
 });
 
 $app->delete('/users/{user_id}', function($request, $response,$args){ 
   $user = User::find($args['user_id']);
   $user->delete(); 
   if($user->exists){
        return $response->withStatus(400);
    } else {
        return $response->withStatus(200);
    }
 
 });

 ////////////Ex3 - users update, bulk create, bulk delete

$app->put('/users/{user_id}', function($request, $response, $args){
    $user = $request->getParsedBodyParam('username', '');
    $_user = User::find($args['user_id']);
    $_user->username = $user;
    if($_user->save()){
        $payload = ['user_id'=>$_user->id, "result"=>"The user has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    } else{
        return $response->withStatus(400);  
    }
 
 });

$app->post('/users/bulk', function($request, $response, $args){
    $payload = $request->getParsedBody(); //convert the json to array
    User::insert($payload); //include save
    return $response->withStatus(201)->withJson($payload);
      
});

$app->delete('/users/bulk', function($request, $response, $args){
    $payload = $request->getParsedBody(); //convert the json to array
    User::trash($payload);
    
    return $response->withStatus(201)->withJson($payload);
      
 });

 ///////////delete bulk options
 //die($payload);
    //User::find($payload);
    //$payload->delete();
    /*foreach($payload as $pay) {
        //User::find()->where(['id'=>$pay['id']])->delete();
        //User::destroy($pay['id']);
        //$user = User::find($pay);
        //$user->delete();
    }*/
    //User::delete($payload);
    //User::whereIn('id', $payload)->delete();
    //User::insert($payload); //include save

$app->run();